
// WaveShowDlg.cpp : 实现文件
//
#include "stdafx.h"
#include "WaveShow.h"
#include "WaveShowDlg.h"
#include "Histrory.h"
#include "afxdialogex.h"
#include <math.h>
#include <time.h>
#include <sstream>
#define random(x) (rand()%x)   //产生随机数
#define pi 3.1415926
#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CWaveShowDlg 对话框



CWaveShowDlg::CWaveShowDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CWaveShowDlg::IDD, pParent)
	, m_time(0)
	, m_level(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CWaveShowDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SHOW, m_control);
	DDX_Control(pDX, IDC_COORD, m_wavewindow);
	DDX_Control(pDX, IDC_HORIZON, m_horizon);
	DDX_Control(pDX, IDC_VERTICAL, m_vertical);
	DDX_Radio(pDX, IDC_TIME1, m_time);
	DDX_Radio(pDX, IDC_LEVEL1, m_level);
	DDX_Control(pDX, IDC_STOREDATA, m_store);
	DDX_Control(pDX, IDC_MSCOMM1, m_mscom);
	DDX_Control(pDX, IDC_COMNUM, m_comid);
	DDX_Control(pDX, IDC_BAUADRATE, m_bauadrate);
}

BEGIN_MESSAGE_MAP(CWaveShowDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SHOW, &CWaveShowDlg::OnBnClickedShow)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_HISTRORY, &CWaveShowDlg::OnBnClickedHistrory)
	ON_BN_CLICKED(IDC_BTN_GRID, &CWaveShowDlg::OnBnClickedBtnGrid)
	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(IDC_STOREDATA, &CWaveShowDlg::OnBnClickedStoredata)
END_MESSAGE_MAP()


// CWaveShowDlg 消息处理程序

BOOL CWaveShowDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO:  在此添加额外的初始化代码
	//初始化数据
	srand((int)time(0));
	m_Low = 0;
	m_High = 40;
	m_now = 0;
	memset(m_lCount, 0, 1024);
	m_over = 0;			
	m_horizon.SetScrollRange(0,100);
	m_horizon.SetScrollPos(50);   //x轴偏移设置为中间
	m_vertical.SetScrollRange(0, 100);
	m_vertical.SetScrollPos(50);  //y轴偏移设置为中间
	m_showgrid = false;  // 显示网格设置为FALSE
	b_store = false;
	count = 1;
	//添加COM
	m_comid.AddString(L"COM1");
	m_comid.AddString(L"COM2");
	m_comid.AddString(L"COM3");
	m_comid.SetCurSel(0);
	//添加波特率
	m_bauadrate.AddString(L"4800");
	m_bauadrate.AddString(L"5600");
	m_bauadrate.AddString(L"9600");
	m_bauadrate.SetCurSel(0); 
	SetTimer(1, 60, NULL);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CWaveShowDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CWaveShowDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CWaveShowDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
void CWaveShowDlg::DrawWave(CDC *pDC){
	UpdateData(TRUE);

	CRect rect;
	CString str;
	int i;
	// 获取绘制坐标的文本框
	CWnd* pWnd = GetDlgItem(IDC_COORD);
	pWnd->GetClientRect(&rect);
	pDC->Rectangle(&rect);

	//获取偏移量
	int m_xslide, m_yslide;
	m_xslide = (m_horizon.GetScrollPos()-50)*4;
	m_yslide = m_vertical.GetScrollPos() - 50;//50为中间位置
	//坐标轴位置
	int m_left, m_top, m_right, m_bottom;
	m_left = rect.left + 20;
	m_top = rect.top + 10;
	m_right = rect.right - 20;
	m_bottom = rect.bottom - 20;
	iTemp = (m_bottom - m_top) / 10;			//步长
	// 创建红色画笔对象
	CPen* pPenRed = new CPen;

	//获取采样频率
	float time;
	switch (m_time){
	case 0:
		time = 0.05;
		break;
	case 1:
		time = 0.25;
		m_High=200;
		break;
	case 2:
		time = 0.5;
		m_High =400;
		break;

	}

	// 红色画笔
	pPenRed->CreatePen(PS_SOLID, 1, RGB(255, 0, 0));

	// 创建蓝色画笔对象
	CPen* pPenBlue = new CPen;

	// 蓝色画笔
	pPenBlue->CreatePen(PS_SOLID, 1, RGB(0, 0, 255));

	// 创建黑色画笔对象
	CPen* pPenblack = new CPen;

	// 黑色画笔
	pPenblack->CreatePen(PS_DOT, 1, RGB(0, 0, 0));

	// 选中当前红色画笔，并保存以前的画笔
	CGdiObject* pOldPen = pDC->SelectObject(pPenRed);

	// 绘制坐标轴
	pDC->MoveTo(m_left, m_top);

	// 垂直轴
	pDC->LineTo(m_left, m_bottom);

	// 水平轴
	pDC->LineTo(m_right, m_bottom);
	// 绘制x轴箭头
	pDC->MoveTo(m_right - 3, m_bottom - 3);
	pDC->LineTo(m_right, m_bottom);
	pDC->LineTo(m_right - 3, m_bottom + 3);

	// 绘制y轴箭头	
	pDC->MoveTo(m_left - 3, m_top + 3);
	pDC->LineTo(m_left, m_top);
	pDC->LineTo(m_left + 3, m_top + 3);

	//选择黑色画笔
	//pDC->SelectObject(pPenblack);	

	//绘制网格
	CString s;
	//s = '0';
	//pDC->TextOut(m_left, m_bottom + 2, s);
	// 绘制X轴刻度
	if (m_showgrid){
	
		for (i = 0; i < m_right - 20; i += 20)
		{

			pDC->MoveTo(m_left + i, m_bottom);
			//绘制网格
			
			pDC->LineTo(m_left + i, m_bottom - 10 * iTemp);
			s.Format(L"%d", i/20);
			pDC->TextOut(m_left + i, m_bottom + 2, s);
		}

		//绘制Y轴刻度
		for (i = 1; i <= 10; i++)
		{
			pDC->MoveTo(m_left, m_bottom - i*iTemp);
			
			pDC->LineTo(m_right, m_bottom - i*iTemp);
			s.Format(L"%d", i);
			pDC->TextOut(m_left - 17, m_bottom - i*iTemp, s);
		}
	}
	if (1 == m_count % 2)
	{
		m_cycctl = 1;
		SetDlgItemText(IDC_SHOW, L"停止显示");
	}
	else
	{
		m_cycctl = 0;
		SetDlgItemText(IDC_SHOW,L"开始显示");
	}
	//m_cycctl = 1;
	count+=2;
	if (count >50) count = 0;
	if (1 == m_cycctl&&m_now<m_High)
	{
		m_now++;
		//m_lCount[m_now] = random(m_bottom);  //模拟数据
		m_lCount[m_now] = sin(count/50.0*2*pi)*m_bottom/3+m_bottom/3+20;
		//m_lCount[m_now] = count>25 ? m_bottom /2 : 20;
		if (b_store && df != NULL)
			df->StoreData(m_lCount[m_now]/m_bottom);
	//	m_lCount[m_now] = current;          //current 为当前从串口读取的数据，读取数据见OnCommMscomm1
		
		//m_lCount[m_now] = m_now*10;
	}
	else if (m_now >= m_High){
		for (int i = 0; i<m_High; i++){
			m_lCount[i] = m_lCount[i + 1];
		}
		m_lCount[m_High] = 90; // 模拟数据

		//	m_lCount[m_High] = current;//current 为当前从串口读取的数据，读取数据见OnCommMscomm1
		m_now = m_High - 1;
	}
	pDC->SelectObject(pPenBlue);

	float xTemp;
	float yTemp;
	float xSave;
	float ySave;

	for (i = m_Low; i<m_High; i++)
	{
		xTemp = (m_left + (float)i / time + m_xslide);
		yTemp = (m_bottom-m_lCount[i])+m_yslide;
		if (i == 1)
		{
			xSave = xTemp;
			ySave = yTemp;
		}
		if (yTemp <= (m_bottom - 10 * iTemp))
			yTemp = m_bottom - 10 * iTemp;
		if (yTemp > m_bottom)
			yTemp = m_bottom;
		if ((xTemp >= (m_left + 0.0001 + m_xslide)) && (xTemp <= (m_left + 700)) && (yTemp != m_bottom))
		{

			pDC->MoveTo(xSave, ySave);
			pDC->LineTo(xTemp, yTemp);
			xSave = xTemp;
			ySave = yTemp;
		}

	}
	pDC->SelectObject(pOldPen);
	delete pPenRed;
	delete pPenBlue;
	delete pPenblack;
	return;
}

void CWaveShowDlg::OnBnClickedShow()
{
	// TODO:  在此添加控件通知处理程序代码
	m_count++;
}

void CWaveShowDlg::UpdateView(CDC *pdc){
	CRect rect;

	// 获取绘制坐标的文本框
	CWnd* pWnd = GetDlgItem(IDC_COORD);
	pWnd->GetClientRect(&rect);
	// 指针
	pDC = pWnd->GetDC();
	//pWnd->Invalidate();
	//pWnd->UpdateWindow();
	//pDC->Rectangle(&rect);
	//DrawWave(pDC);

	//内存绘图

}

void CWaveShowDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	// TODO: Add your message handler code here and/or call default
	CRect rect;

	// 获取绘制坐标的文本框
	CWnd* pWnd = GetDlgItem(IDC_COORD);

	pWnd->GetClientRect(&rect);
	// 指针
	pDC = pWnd->GetDC();
	//pWnd->Invalidate();
	//pWnd->UpdateWindow();
	//pDC->Rectangle(&rect);
	//DrawWave(pDC);

	//内存绘图
	CBitmap memBitmap;
	CBitmap* pOldBmp = NULL;
	memDC.CreateCompatibleDC(pDC);
	memBitmap.CreateCompatibleBitmap(pDC, rect.right - rect.left, rect.bottom - rect.top);
	pOldBmp = memDC.SelectObject(&memBitmap);
	DrawWave(&memDC);
	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldBmp);
	memDC.DeleteDC();
	memBitmap.DeleteObject();

	CDialog::OnTimer(nIDEvent);
	CDialogEx::OnTimer(nIDEvent);
}


void CWaveShowDlg::OnBnClickedHistrory()
{
	// TODO:  在此添加控件通知处理程序代码
	b_store = false;
	delete df;
	CHistrory dlg;
	dlg.DoModal();
}

void CWaveShowDlg::OnBnClickedBtnGrid()
{
	m_showgrid = m_showgrid ? false : true;
	
}

void CWaveShowDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值

	int iMin = 0, iMax = 0;
	pScrollBar->GetScrollRange(&iMin, &iMax);
	int pos = m_vertical.GetScrollPos();
	int iPos = nPos;
	switch (nSBCode)
	{
	case SB_THUMBTRACK:           //拖拽过程
		if (iPos < pos && iPos >= iMin)
		{
			pScrollBar->SetScrollPos(iPos, TRUE);
			m_vertical.SetScrollPos(pos);

		}
		else if (iPos > pos && iPos <= iMax)
		{
			pScrollBar->SetScrollPos(iPos, TRUE);
			m_vertical.SetScrollPos(pos);

		}
		m_vertical.SetScrollPos(pos);
		/*	s.Format(_T("%d"), m_scroller.GetScrollPos());
		m_scroller.SetScrollPos(pos);
		GetDlgItem(IDC_STATIC)->SetWindowText(s);*/
		//InitData();
		break;

		// 如果向下滚动一列，则pos减1  
	case SB_LINEDOWN:
		pos -= 1;
		break;
		// 如果向右滚动一列，则pos加1  
	case SB_LINEUP:
		pos += 1;
		break;
		// 如果向左滚动一页，则pos减10  
	case SB_PAGEDOWN:
		pos -= 10;
		break;
		// 如果向右滚动一页，则pos加10  
	case SB_PAGEUP:
		pos += 10;
		break;
		// 如果滚动到最左端，则pos为1  
	case SB_TOP:
		pos = iMin;
		break;
		// 如果滚动到最右端，则pos为100  
	case SB_BOTTOM:
		pos = iMax;
		break;
		// 如果拖动滚动块滚动到指定位置，则pos赋值为nPos的值  
	case SB_THUMBPOSITION:		//拖拽过程
		pos = nPos;
		break;
	default:
		return;
	}
	m_vertical.SetScrollPos(pos);

	CDialogEx::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CWaveShowDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	int iMin = 0, iMax = 0;
	pScrollBar->GetScrollRange(&iMin, &iMax);
	int pos = m_horizon.GetScrollPos();
	int iPos = nPos;
	switch (nSBCode)
	{
	case SB_THUMBTRACK:           //拖拽过程
		if (iPos < pos && iPos >= iMin)
		{
			pScrollBar->SetScrollPos(iPos, TRUE);
			m_horizon.SetScrollPos(pos);

		}
		else if (iPos > pos && iPos <= iMax)
		{
			pScrollBar->SetScrollPos(iPos, TRUE);
			m_horizon.SetScrollPos(pos);

		}
		m_horizon.SetScrollPos(pos);
		/*	s.Format(_T("%d"), m_scroller.GetScrollPos());
		m_scroller.SetScrollPos(pos);
		GetDlgItem(IDC_STATIC)->SetWindowText(s);*/
		//InitData();
		break;

		// 如果向左滚动一列，则pos减1  
	case SB_LINELEFT:
		pos -= 1;
		break;
		// 如果向右滚动一列，则pos加1  
	case SB_LINERIGHT:
		pos += 1;
		break;
		// 如果向左滚动一页，则pos减10  
	case SB_PAGELEFT:
		pos -= 10;
		break;
		// 如果向右滚动一页，则pos加10  
	case SB_PAGERIGHT:
		pos += 10;
		break;
		// 如果滚动到最左端，则pos为1  
	case SB_LEFT:
		pos = 1;
		break;
		// 如果滚动到最右端，则pos为100  
	case SB_RIGHT:
		pos = iMax;
		break;
		// 如果拖动滚动块滚动到指定位置，则pos赋值为nPos的值  
	case SB_THUMBPOSITION:		//拖拽过程
		pos = nPos;
		break;
	default:
		return;
	}
	m_horizon.SetScrollPos(pos);
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CWaveShowDlg::OnBnClickedStoredata()
{
	b_store = true;
	BOOL isOpen = FALSE;        //是否打开(否则为保存)  
	CString defaultDir = L"C:\\Users\\Default\\AppData";   //默认打开的文件路径  
	CString fileName = L"data.dat";         //默认打开的文件名  
	CString filter = L"文件 (*.dat; *.txt;)";   //文件过虑的类型  
	CFileDialog openFileDlg(isOpen, defaultDir, fileName, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, filter, NULL);
	INT_PTR result = openFileDlg.DoModal();
	CString filePath = defaultDir + "\\" + fileName;
	if (result == IDOK) {
		filePath = openFileDlg.GetPathName();
		this->filename = filePath;
		df = new DataFile(filename); 
	}
}
BEGIN_EVENTSINK_MAP(CWaveShowDlg, CDialogEx)
	ON_EVENT(CWaveShowDlg, IDC_MSCOMM1, 1, CWaveShowDlg::OnCommMscomm1, VTS_NONE)
END_EVENTSINK_MAP()

//初始化串口
void CWaveShowDlg::initMscom1(){
	int flag = m_comid.GetCurSel();
	CString rate;
	m_bauadrate.GetWindowTextW(rate);
	if (m_mscom.get_PortOpen())   //如果串口是打开的
	{
		m_mscom.put_PortOpen(FALSE);
	}
	m_mscom.put_CommPort(flag); //选择COM
	m_mscom.put_InBufferSize(1024); //接收缓冲区
	m_mscom.put_OutBufferSize(1024);//发送缓冲区
	m_mscom.put_InputLen(0);//设置当前接收区数据长度为0,表示全部读取
	m_mscom.put_InputMode(1);//以二进制方式读写数据
	m_mscom.put_RThreshold(1);//接收缓冲区有1个及1个以上字符时，将引发接收数据的OnComm
	CString setting;
	setting.Format(L"%s,n,8,1", rate);
	m_mscom.put_Settings(setting);//波特率9600无检验位，8个数据位，1个停止位
	if (!m_mscom.get_PortOpen())//如果串口没有打开则打开
	{
		m_mscom.put_PortOpen(TRUE);//打开串口
		AfxMessageBox(_T("串口打开成功"));
	}
	else
	{
		m_mscom.put_OutBufferCount(0);
		AfxMessageBox(_T("串口打开失败"));
	}

}
//从串口接受数据
void CWaveShowDlg::OnCommMscomm1()
{
	static unsigned int cnt = 0;
	VARIANT variant_inp;
	COleSafeArray safearray_inp;
	long len, k;
	unsigned int data[1024] = { 0 };
	byte rxdata[1024]; //设置 BYTE 数组
	CString strtemp;
	if (m_mscom.get_CommEvent() == 2) //值为 2 表示接收缓冲区内有字符
	{
		cnt++;
		variant_inp = m_mscom.get_Input(); //读缓冲区消息
		safearray_inp = variant_inp; ///变量转换
		len = safearray_inp.GetOneDimSize(); //得到有效的数据长度
		for (k = 0; k<len; k++)
		{
			safearray_inp.GetElement(&k, rxdata + k);
		}
	}
	current = rxdata[len];
}
