
// WaveShowDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "DataFile.h"
#include "CMSComm.h"

// CWaveShowDlg 对话框
class CWaveShowDlg : public CDialogEx
{
// 构造
public:
	CWaveShowDlg(CWnd* pParent = NULL);	// 标准构造函数
	void DrawWave(CDC *pDC);
	void UpdateView(CDC *pdc);
// 对话框数据
	enum { IDD = IDD_WAVESHOW_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CButton m_control;
private:
	int m_cycctl;
	int m_count;
	int iTemp;
	int m_now;
	float m_lCount[1024];
	int m_Low;
	int m_High;
	int m_over;
	CDC memDC;
	CDC* pDC;
	
public:
	CStatic m_wavewindow;
	afx_msg void OnBnClickedShow();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	
	afx_msg void OnBnClickedHistrory();
	CScrollBar m_horizon;           //水平偏移
	CScrollBar m_vertical;			//垂直偏移
	int m_time;						//采集数据频率
	int m_level;					//等级
	bool m_showgrid;				//是否显示网格
	CString filename;               // 存储数据文件名
	CButton m_store;				//是否存储数据
	bool   b_store;
	DataFile *df;					//操作文件对象
	afx_msg void OnBnClickedBtnGrid();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	
	afx_msg void OnBnClickedStoredata();
	CMSComm m_mscom;
	DECLARE_EVENTSINK_MAP()
	float current;
	int count;
	void initMscom1();
	void OnCommMscomm1();
	CComboBox m_comid;
	CComboBox m_bauadrate;
};
