//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 WaveShow.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_WAVESHOW_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_HISTRORY                    129
#define IDC_SHOW                        1001
#define IDC_COORD                       1002
#define IDC_SCROLLBAR1                  1004
#define IDC_HISTRORY                    1005
#define IDC_OPEN_FILE                   1006
#define IDC_TITLE                       1007
#define IDC_WAVESHOW                    1008
#define IDC_HORIZON                     1012
#define IDC_VERTICAL                    1013
#define IDC_BTN_GRID                    1014
#define IDC_TIME1                       1015
#define IDC_TIME2                       1016
#define IDC_TIME3                       1017
#define IDC_LEVEL1                      1018
#define IDC_LEVEL2                      1019
#define IDC_LEVEL3                      1020
#define IDC_STOREDATA                   1021
#define IDC_MSCOMM1                     1022
#define IDC_COMNUM                      1024
#define IDC_COMBO2                      1025
#define IDC_BAUADRATE                   1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
