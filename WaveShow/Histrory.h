#pragma once
#include "afxwin.h"


// CHistrory 对话框

class CHistrory : public CDialogEx
{
	DECLARE_DYNAMIC(CHistrory)

public:
	CHistrory(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CHistrory();
	void InitData();
	void DrawWave(CDC *pDC);
// 对话框数据
	enum { IDD = IDD_HISTRORY };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOpenFile();
	CScrollBar m_scroller;
//	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
private:
	CDC memDC;
	CDC* pDC;
	float m_lCount[1024];
	int m_Low;
	int m_High;
	int temp;
public:
	afx_msg void OnStnClickedWaveshow();
};
