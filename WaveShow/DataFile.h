#pragma once
class DataFile
{
public:
	
	DataFile();
	DataFile(CString filename);
	float *GetData();
	float *GetData(CString);
	bool StoreData(float data);
	~DataFile();
private:
	CStdioFile file;
	CString filename;
	int DataSize;
};

