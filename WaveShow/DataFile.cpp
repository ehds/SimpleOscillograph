#include "stdafx.h"
#include "DataFile.h"

DataFile::DataFile(){}
DataFile::DataFile(CString filename)
{
	this->filename = filename;  //初始化文件名
	
	DataSize = 1024;
	file.Open(filename,CFile::modeCreate|CFile::modeReadWrite);
	
}
DataFile::~DataFile()
{	
	if (file != NULL) file.Close();   //如果文件未关闭，关闭文件
}
float* DataFile::GetData(CString filename){
	CStdioFile fileread;
	fileread.Open(filename, CFile::modeRead);
	float *data = new float[DataSize];   //读取DataSize个数据
	CString line;
	if (fileread){
		for (size_t i = 0; i < DataSize; i++)
		{
			fileread.ReadString(line);
			if (line != "")
			{
				data[i] = _ttof(line);
			}
			else break;
		}
	}
	fileread.Close();
	return data;
}
bool DataFile::StoreData(float data){

	if (file){
		CString line;
		CString str;
		str.Format(L"%f\n", data);
		file.Seek(0, CFile::end);		//追加数据
		file.WriteString(str);
	}
	return true;
}