// Histrory.cpp : 实现文件
//

#include "stdafx.h"
#include "WaveShow.h"
#include "Histrory.h"
#include "afxdialogex.h"
#include "DataFile.h"
#include <math.h>
#include <time.h>

#define random(x) (rand()%x)

// CHistrory 对话框

IMPLEMENT_DYNAMIC(CHistrory, CDialogEx)

CHistrory::CHistrory(CWnd* pParent /*=NULL*/)
	: CDialogEx(CHistrory::IDD, pParent)
{

}

CHistrory::~CHistrory()
{
}

void CHistrory::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SCROLLBAR1, m_scroller);
}


BEGIN_MESSAGE_MAP(CHistrory, CDialogEx)
	ON_BN_CLICKED(IDC_OPEN_FILE, &CHistrory::OnBnClickedOpenFile)
//	ON_WM_VSCROLL()
	ON_WM_HSCROLL()
	ON_STN_CLICKED(IDC_WAVESHOW, &CHistrory::OnStnClickedWaveshow)
END_MESSAGE_MAP()


// CHistrory 消息处理程序

void CHistrory::InitData(){
	
	CRect rect;
	// 获取绘制坐标的文本框
	CWnd* pWnd = GetDlgItem(IDC_WAVESHOW);
	pWnd->GetClientRect(&rect);
	// 指针
	pDC = pWnd->GetDC();
	pDC->Rectangle(&rect);
	//rect.MoveToX(temp++);
	/*pWnd->Invalidate();
	pWnd->UpdateWindow();*/
	
		CBitmap memBitmap;
		CBitmap* pOldBmp = NULL;
		memDC.CreateCompatibleDC(pDC);
		memBitmap.CreateCompatibleBitmap(pDC, rect.right - rect.left, rect.bottom - rect.top);
		pOldBmp = memDC.SelectObject(&memBitmap);
		DrawWave(&memDC);
		pDC->BitBlt(0, 0, rect.Width(), rect.Height(), &memDC, 0, 0, SRCCOPY);
		memDC.SelectObject(pOldBmp);
		memDC.DeleteDC();
		memBitmap.DeleteObject();
	
	
	
}
void CHistrory::OnBnClickedOpenFile()
{
	 TCHAR szFilter[] = _T("文本文件(*.dat)|*.txt|所有文件(*.*)|*.*||");
	// 构造打开文件对话框
	CFileDialog fileDlg(TRUE, _T("dat"), NULL, 0, szFilter, this);
	CString strFilePath;
	float *data = NULL;
	// 显示打开文件对话框
	if (IDOK == fileDlg.DoModal())
	{
	// 如果点击了文件对话框上的“打开”按钮，则将选择的文件路径显示到编辑框里
	strFilePath = fileDlg.GetPathName();
	DataFile df;
	data = df.GetData(strFilePath);
	
	//DataFile df(strFilePath);
	//data = df.GetData();   //获取数据
	/*CString line;
	line.Format(L"%f", data[1]);*/
	
	}

	CRect rect;
	// 获取绘制坐标的文本框
	CWnd* pWnd = GetDlgItem(IDC_WAVESHOW);
	pWnd->GetClientRect(&rect);
	// 指针
	pDC = pWnd->GetDC();
	pDC->Rectangle(&rect);
	int bottom = rect.bottom;
	for (size_t q = 0; q < 512; q++)
	{
		m_lCount[q] = random(rect.bottom);
	}
	InitData();
}


BOOL CHistrory::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	srand((int)time(0));
	m_Low = 0;
	temp = 0;
	m_High = 1024;
	m_scroller.SetScrollRange(0, 512,TRUE);
	m_scroller.SetScrollPos(0);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常:  OCX 属性页应返回 FALSE
}

void CHistrory::DrawWave(CDC *pDC){
	UpdateData(TRUE);
	CRect rect;
	CString str;
	
	int iMin = 0, iMax = 0;
	
	m_scroller.GetScrollRange(&iMin, &iMax);
	int temp = m_High/iMax*m_scroller.GetScrollPos();
	int i;
	// 获取绘制坐标的文本框
	CWnd* pWnd = GetDlgItem(IDC_WAVESHOW);
	pWnd->GetClientRect(&rect);
	pDC->Rectangle(&rect);

	int m_left, m_top, m_right, m_bottom;
	m_left = rect.left + 20;
	m_top = rect.top + 10;
	m_right = rect.right - 20;
	m_bottom = rect.bottom - 20;
	int iTemp = (m_bottom - m_top) / 10;			//步长
	int step = (m_right - m_left) / 10;
	CPen* pPenRed = new CPen;

	// 红色画笔
	pPenRed->CreatePen(PS_SOLID, 1, RGB(255, 0, 0));

	// 创建蓝色画笔对象
	CPen* pPenBlue = new CPen;

	// 蓝色画笔
	pPenBlue->CreatePen(PS_SOLID, 1, RGB(0, 0, 255));

	// 创建黑色画笔对象
	CPen* pPenblack = new CPen;

	// 黑色画笔
	pPenblack->CreatePen(PS_DOT, 1, RGB(0, 0, 0));

	// 选中当前红色画笔，并保存以前的画笔
	CGdiObject* pOldPen = pDC->SelectObject(pPenRed);

	// 绘制坐标轴
	pDC->MoveTo(m_left, m_top);

	// 垂直轴
	pDC->LineTo(m_left, m_bottom);


	// 水平轴
	pDC->LineTo(m_right, m_bottom);
	
	// 绘制x轴箭头
	pDC->MoveTo(m_right - 3, m_bottom - 3);
	pDC->LineTo(m_right, m_bottom);
	pDC->LineTo(m_right - 3, m_bottom + 3);

	// 绘制y轴箭头	
	pDC->MoveTo(m_left - 3, m_top + 3);
	pDC->LineTo(m_left, m_top);
	pDC->LineTo(m_left + 3, m_top + 3);
	float xTemp;
	float yTemp;
	float xSave;
	float ySave;
	float time = 0.05;
	for (i = temp; i<temp + step; i++)
	{
		xTemp = (m_left + (float)(i-temp+1)*20);
		yTemp = (m_bottom - m_lCount[i]);
		if (i == temp)
		{
			xSave = xTemp;
			ySave = yTemp;
		}
		if (yTemp <= (m_bottom - 10 * iTemp))
		yTemp = m_bottom - 10 * iTemp;
		if (yTemp > m_bottom)
		yTemp = m_bottom;
		if ((xTemp >= (m_left + 0.0001)) && (xTemp <= (m_right-20)) && (yTemp != m_bottom))
		{
			pDC->MoveTo(xSave, ySave);
			pDC->LineTo(xTemp, yTemp);
			xSave = xTemp;
			ySave = yTemp;
		}

	}

	pDC->SelectObject(pOldPen);
	delete pPenRed;
	delete pPenBlue;
	delete pPenblack;

	return;

}
void CHistrory::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	// TODO:  在此添加消息处理程序代码和/或调用默认值
	int pos = m_scroller.GetScrollPos();    // 获取水平滚动条当前位置  
	int iPos = nPos;
	int iMin = 0, iMax = 0;
	CString s;
	pScrollBar->GetScrollRange(&iMin, &iMax);
	switch (nSBCode)
	{
	case SB_THUMBTRACK:
		if (iPos < pos && iPos >= iMin)
		{
			pScrollBar->SetScrollPos(iPos, TRUE);

		}
		else if (iPos > pos && iPos <= iMax)
		{
			pScrollBar->SetScrollPos(iPos, TRUE);

		}
		m_scroller.SetScrollPos(pos);
	/*	s.Format(_T("%d"), m_scroller.GetScrollPos());
		m_scroller.SetScrollPos(pos);
		GetDlgItem(IDC_STATIC)->SetWindowText(s);*/
		//InitData();
		
		break;
		
		// 如果向左滚动一列，则pos减1  
	case SB_LINELEFT:
		pos -= 1;
		break;
		// 如果向右滚动一列，则pos加1  
	case SB_LINERIGHT:
		pos += 1;
		break;
		// 如果向左滚动一页，则pos减10  
	case SB_PAGELEFT:
		pos -= 10;
		break;
		// 如果向右滚动一页，则pos加10  
	case SB_PAGERIGHT:
		pos += 10;
		break;
		// 如果滚动到最左端，则pos为1  
	case SB_LEFT:
		pos = 1;
		break;
		// 如果滚动到最右端，则pos为100  
	case SB_RIGHT:
		pos = iMax;
		break;
		// 如果拖动滚动块滚动到指定位置，则pos赋值为nPos的值  
	case SB_THUMBPOSITION:
		pos = nPos;
		break;
	default:
		return;
	}
	// 设置滚动块位置  
	m_scroller.SetScrollPos(pos);
	InitData();
	CDialogEx::OnHScroll(nSBCode, nPos, pScrollBar);

}


void CHistrory::OnStnClickedWaveshow()
{
	// TODO:  在此添加控件通知处理程序代码
}
